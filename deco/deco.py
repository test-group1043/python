import time

def auth(func):
    def deco(*args, **kwargs):
        tag = True
        while tag:
            u_name = input('name >>>')
            # with open('db.txt', 'rt', encoding='utf-8') as f1:
            l1 = [i for i in open('deco/db.txt')]
            l2 = l1[0].split(':')
            if u_name == l2[0]:
                u_pwd = input('password >>>')
                if u_pwd == l2[1]:
                    print('Login Successful')
                    tag = False
                    func(*args, **kwargs)
                else:
                    print('Wrong Password')
            else:
                print('Wrong Username')
    return deco

def cal_time(func):
    def deco(*args, **kwargs):
        start_time = time.time()
        func(*args, **kwargs)
        stop_time = time.time()
        print(f'Running {stop_time-start_time} s')
    return deco

@auth
@cal_time
def func1():
    print('this is func1')
    time.sleep(3)

@auth
@cal_time
def func2(x):
    print(f'this is func2 {x}')
    time.sleep(3)

func1()
func2('weller')