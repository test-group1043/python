# 具有__iter__()方法的是可迭代对象
# 具有 __iter()__ 和__next()__方法的是迭代器对象
# 迭代器对象一定是可迭代对象 可迭代对象不一定是迭代器对象
# for原理: 将循环中的对象先使用__iter()__方法转换为迭代器对象, 再从迭代器对象中依次取值, 在取完后捕获异常结束循环

# 生成器其实就是迭代器, 只是迭代器针对的是内置的对象, 生成器针对的是自己造的对象, 通过yield进行返回并暂停

# def func1():
#     yield 1
#     yield 2
#     yield 3
#     yield 4
#     yield 5

# print(type(func1()))
# print(func1().__next__())
# for i in func1():
#     print(i)

# 三元表达式
# x = 1
# y = 2
# print(x if x > y else y)

# 列表生成式
# s1='hello'
# l = [i for i in s1]
# print(l)

# 字典生成式
# s1 = 'hello'
# s2 = 'watch'
# z1 = zip(s1, s2)
# for k,v in z1:
#     print(f'{k}:{v}')
#     print(f'{k}:{v}')
#     print(f'{k}:{v}')
# d1 = {k:v for k,v in zip(s1, s2)}
# print(d1)

# 统计文件最长字符串
l1 = [len(i) for i in open('/declare/1.txt')]
print(l1)