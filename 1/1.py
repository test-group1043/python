# n = 1
# while n <= 12:
#     if n == 6 or n == 10:
#         n+=1
#         continue
#     else:
#         print(n)
#         n+=1

# while n <=100:
#     if n % 2 == 0:
#         print(n)
#     n+=1

# n = 2
# num = 0
# while n <= 100:
#     if n % 2 == 0:
#         num+=n
#     else:
#         num-=n
#     n+=1
# print(num)

# i = 1
# for i in range(i,10):
#     for j in range(i,10):
#         print(f'{i}*{j}={i*j}', end=' ')
#         j+=1
#     print()
#     i+=1

# for i in range(1,6):
#     print((('*'*(2*i-1))).center(9, ' '))
#     i+=1


# '''
# 让用户输入用户名密码, 可以支持多个用户登录 (提示，通过列表存多个账户信息)
# 认证成功后显示欢迎信息
# 输错三次后退出程序, 用户3次认证失败后，退出程序，再次启动程序尝试登录时，还是锁定状态（提示:需把用户锁定的状态存到文件里）
# '''
# n = 0
# f1 = '1/lock.txt'
# f2 = '1/db.txt'

# flag = True
# while n <3 and flag:

#     with open(f1, 'rt', encoding='utf-8') as f:
#         if f.readline() == 'Account Locked':
#             print('Account Locked! Exit...')
#             flag = False
#         else:
#             u_name = input('name >>>')
#             u_pwd = input('password >>>')

#             # if u_name == 'weller' and u_pwd == '123':
#             #     print('Login Successful')
#             #     flag = False
#             # else:
#             #     print('Wrong Info')
#             #     n+=1

#             for i in open(f2):
#                 if u_name in i.strip('\n').split(':') and u_pwd in i.strip('\n').split(':'):
#                     print('Login Successful')
#                     flag = False
#                     break
#             else:
#                 print('Wrong Info')
#                 n+=1


# if n >= 3:
#     print('Account Locked')
#     with open(f1, 'wt', encoding='utf-8') as f:
#         f.write('Account Locked')