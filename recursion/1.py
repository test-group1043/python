# 递归是不断的直接或者间接自我调用 一个阶段是递推(一层一层递归调用下去) 一个阶段是回溯(必须有明确条件)
# def bar():
#     print('this is bar function')
#     foo()

# def foo():
#     print('this is foo function')
#     bar()

# foo()

# 5个人年龄
# n = 26
# l = 1
# while l <= 5:
#     n = n + 2
#     l+=1
# print(n, l)

# def age(n):
#     if n == 1:
#         return 26
#     else:
#         return age(n-1)+2

# print(age(5))

# l1 = [1,[2,[3,[4,[5,[6,[7,[8,[9]]]]]]]]]
# print(l1[1][1][1][0])
# i = 0
# while i < 9:
#     print(f'{l1[{i}]}')

# def func(l):
#     for i in l:
#         if type(i) != list:
#             print(i)
#         else:
#             func(i)

# func(l1)

nums = [1,2,3,4,5,6,7,8,9,99,999,9999,999999]

def search(i, l):
    if len(l) == 0:
        print('Not Exist')
    n = len(l) // 2
    print(i, n, l)
    if i > l[n]: 
        search(i, l[n+1:])
    elif i < l[n]:
        search(i, l[:n])
    elif i == l[n]:
        print('Found')

search(100, nums)