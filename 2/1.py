# name = " aleX"
# # 1)    移除 name 变量对应的值两边的空格,并输出处理结果
# print(name.strip())
# # 2)    判断 name 变量对应的值是否以 "al" 开头,并输出结果
# print(name.startswith('al'))
# # 3)    判断 name 变量对应的值是否以 "X" 结尾,并输出结果
# print(name.endswith('X'))
# # 4)    将 name 变量对应的值中的 “l” 替换为 “p”,并输出结果
# print(name.replace('l', 'p'))
# # 5)    将 name 变量对应的值根据 “l” 分割,并输出结果。
# print(name.split('l'))
# # 6)    将 name 变量对应的值变大写,并输出结果
# print(name.upper())
# # 7)    将 name 变量对应的值变小写,并输出结果
# print(name.lower())
# # 8)    请输出 name 变量对应的值的第 2 个字符?
# print(name[1])
# # 9)    请输出 name 变量对应的值的前 3 个字符?
# print(name[:3])
# # 10)    请输出 name 变量对应的值的后 2 个字符?
# print(name[-2:])
# # 11)    请输出 name 变量对应的值中 “e” 所在索引位置?
# print(name.find('e'))
# # 12)    获取子序列,去掉最后一个字符。如: oldboy 则获取 oldbo
# print(name[:len(name)-1])

# 1. 有列表data=['alex',49,[1900,3,18]]，分别取出列表中的名字，年龄，出生的年，月，日赋值给不同的变量
# data=['alex',49,[1900,3,18]]
# name = data[0]
# age = data[1]
# birth_y = data[2][0]
# birth_m = data[2][1]
# birth_d = data[2][2]
# print(name,age,birth_y,birth_m,birth_d)

# 2. 用列表模拟队列
# l = []
# for i in range(3):
#     l.append(i)
#     print(l)

# i = 0
# while i < len(l):
#     print(l[i])
#     i+=1


# 3. 用列表模拟堆栈
# l = []
# for i in range(3):
#     l.append(i)
#     print(l)

# i = 0
# while i < len(l):
#     print(l.pop())
#     i+=1


# 4. 有如下列表，请按照年龄排序（涉及到 匿名函数 ）
# l=[
#     {'name':'alex','age':84},
#     {'name':'oldboy','age':73},
#     {'name':'egon','age':18},
# ]

# print(sorted(l, key=lambda i: i['age']))

# 实现打印商品详细信息，用户输入商品名和购买个数，则将商品名，价格，购买个数加入购物列表，如果输入为空或其他非法输入则要求用户重新输入　
# msg_dic = {
#  'apple': 10,
#  'tesla': 100000,
#  'mac': 3000,
#  'lenovo': 30000,
#  'chicken': 10,
# }

# cart = []
# flag = True
# while flag:
#     s_name = input('name >>> ')
#     if s_name in msg_dic.keys():
#         s_count = input('count >>> ')
#         if s_count.isdigit() and int(s_count) > 0:
#             s_product = {s_name:s_count}
#             cart.append(s_product)
#             print(cart)
#         else:
#             print('count type not correct, input again: ')
#     elif s_name == 'q':
#         flag = False
#     else:
#         print('name not in list, input again: ')

# 1 有如下值集合 [11,22,33,44,55,66,77,88,99,90...]，将所有大于 66 的值保存至字典的第一个key中，将小于 66 的值保存至第二个key的值中
# 即： {'k1': 大于66的所有值, 'k2': 小于66的所有值}
# list1 = [11,22,33,44,55,66,77,88,99,90,99]
# dict1 = {'k1':[], 'k2':[]}
# # l1 = []
# # l2 = []
# for i in list1:
#     if i > 66:
#         dict1['k1'].append(i)
#     else:
#         dict1['k2'].append(i)
# print(dict1)

# 2 统计s='hello alex alex say hello sb sb'中每个单词的个数
s='hello alex alex say hello sb sb'
# list1 = s.split(' ')
# print(list1)
dict1 = {}
for i in s.split(' '):
    if i in dict1:
        dict1[i] += 1
    else:
        dict1[i] = 1
print(dict1)
